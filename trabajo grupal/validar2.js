const asignatura = document.getElementById("materiatut");
const curso = document.getElementById("cursotut");
const fecha = document.getElementById("fechatut");
const numestudiantes = document.getElementById("estudiantestut");
const parrafo = document.getElementById("warnings");

form.addEventListener("submit", e=>{
    let warnings = ""
    let entrar = false 
    parrafo.innerHTML = ""
    
    if(asignatura.value.length <4){
        warnings += `La asignatura que ingresó no es válida. La longitud de la misma debe ser mayor a 2 dígitos. <br>`
    }
    if(curso.value.length <2 ){
        warnings += ` <br> El curso que indicó no es válido. La longitud debe ser mayor a 2 dígitos. <br>`
    }

    if(fecha.value.length <8 ){
        warnings += ` <br> La fecha que indicó no es válida. La longitud debe ser mayor a 8 dígitos. <br>`
    }

    if(numestudiantes.value.length <1 ){
        warnings += ` <br> La cantidad que ingresó no es válida. La longitud debe ser mayor a 1 dígito. <br>`
        entrar = true
    }

    if(entrar){
        parrafo.innerHTML = warnings
        e.preventDefault()
    }
})
