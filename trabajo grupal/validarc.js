const nombrecont = document.getElementById("nombrecon");
const correocont = document.getElementById("correocon");
const tlfcont = document.getElementById("tlfcon");
const msjcont = document.getElementById("msjcon");
const parrafo = document.getElementById("warnings");

form.addEventListener("submit", e=>{
    let warnings = ""
    let entrar = false 
    parrafo.innerHTML = ""
    
    if(nombrecont.value.length <3){
        warnings += ` El nombre que ingresó no es válido. La longitud debe ser mayor a 3 dígitos. <br>`
        entrar = true
    }
    if(correocont.value.length <5 ){
        warnings += ` <br> El correo electrónico ingresado es inválido. Su longitud debe ser mayor a 5 dígitos. <br>`
        entrar = true
    }
    if(tlfcont.value.length <10 || tlfcont.value.length >10 ){
        warnings += ` <br> El número telefónico debe contener estrictamente 10 dígitos, no puede ser ni mayor o menor. <br>`
        entrar = true
    }
    if(msjcont.value.length <20 ){
        warnings += ` <br> El mensaje no puede sobrepasar los 50 dígitos. <br>`
        entrar = true
    }

    if(entrar){
        parrafo.innerHTML = warnings
        e.preventDefault()
    }
})