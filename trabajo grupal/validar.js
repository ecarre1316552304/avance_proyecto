const nombre = document.getElementById("name");
const pass = document.getElementById("password");
const form = document.getElementById("form");
const parrafo = document.getElementById("warnings");

form.addEventListener("submit", e=>{
    let warnings = ""
    let entrar = false 
    parrafo.innerHTML = ""
    
    if(nombre.value.length <10 || nombre.value.length >10){
        warnings += `El usuario ingresado no es valido. Usted ingresó una cédula con una longitud mayor o menor a 10 dígitos. <br>`
    }
    if(pass.value.length <8){
        warnings += ` <br> La contraseña que ingresó no es valida. La longitud de la misma debe ser de 8 o más dígitos. <br>`
        entrar = true
    }

    if(entrar){
        parrafo.innerHTML = warnings
        e.preventDefault()
    }
})
